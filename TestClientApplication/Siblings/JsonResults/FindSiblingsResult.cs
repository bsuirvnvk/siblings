﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Siblings.JsonResults
{
    public class FindSiblingsResult
    {
        public string Message { get; set; }
        public string[] Images { get; set; }
    }
}