﻿(function () {

    var width = 480;    
    var height = 0;
    var streaming = false;

    var video = null;
    var canvas = null;
    var photo = null;
    var startbutton = null;

    function startup() {
        video = document.getElementById('video');
        canvas = document.getElementById('canvas');
        photo = document.getElementById('photo');
        startbutton = document.getElementById('startbutton');

        navigator.getMedia = (navigator.getUserMedia ||
                               navigator.webkitGetUserMedia ||
                               navigator.mozGetUserMedia ||
                               navigator.msGetUserMedia);

        navigator.getMedia(
          {
              video: true,
              audio: false
          },
          function (stream) {
              if (navigator.mozGetUserMedia) {
                  video.mozSrcObject = stream;
              } else {
                  var vendorURL = window.URL || window.webkitURL;
                  video.src = vendorURL.createObjectURL(stream);
              }
              video.play();
          },
          function (err) {
              console.log("An error occured! " + err);
          }
        );

        video.addEventListener('canplay', function (ev) {
            if (!streaming) {
                height = video.videoHeight / (video.videoWidth / width);

                if (isNaN(height)) {
                    height = width / (4 / 3);
                }

                video.setAttribute('width', width);
                video.setAttribute('height', height);
                canvas.setAttribute('width', width);
                canvas.setAttribute('height', height);
                streaming = true;
            }
        }, false);

        startbutton.addEventListener('click', function (ev) {
            takepicture();
            ev.preventDefault();
        }, false);

        clearphoto();
    }

    function clearphoto() {
        var context = canvas.getContext('2d');
        context.fillStyle = "#AAA";
        context.fillRect(0, 0, canvas.width, canvas.height);

        var data = canvas.toDataURL('image/png');
        photo.setAttribute('src', data);
    }

    function takepicture() {
        var context = canvas.getContext('2d');
        if (width && height) {
            canvas.width = width;
            canvas.height = height;
            context.drawImage(video, 0, 0, width, height);

            var data = canvas.toDataURL('image/jpg');

            photo.setAttribute('src', data);

            $.ajax(
                {
                    type: 'POST',
                    url: '/FindSiblings/FindSiblings',
                    data: '{ "fileUpload" : "' + data.replace(/^data:image\/(png|jpg);base64,/, "") + '" }',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        imageProcessingSuccess(data);
                    }
                }
            );

        } else {
            clearphoto();
        }
    }

    function imageProcessingSuccess(data) {
        switch (data.Message) {
        case 'OK':
            var imagesArray = data.Images;

            $('.siblings').children().remove();
            for (var i = 0; i < imagesArray.length; i++) {
                $('.siblings').append($('<img class="siblings-image" src="' + 'data:image/jpg;base64,' + imagesArray[i] + '" style="float: left"\>'));
            }

            break;
        default:
            alert(data.Message);
        }
    }

    window.addEventListener('load', startup, false);
})();