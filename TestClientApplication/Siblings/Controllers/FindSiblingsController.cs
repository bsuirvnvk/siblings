﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.Mvc;
using CompareFace;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Microsoft.AspNet.Identity;
using Siblings.JsonResults;
using Siblings.Global;

namespace Siblings.Controllers
{
    [Authorize]
    public class FindSiblingsController : Controller
    {
        [System.Web.Http.HttpPost]
        public JsonResult FindSiblings(string fileUpload)
        {
            JsonResult result = Json(new FindSiblingsResult()
            {
                Message = "NullOrEmptyImage"
            });

            var random = new Random();

            if (!String.IsNullOrEmpty(fileUpload))
            {
                byte[] imageBytes = Convert.FromBase64String(fileUpload);

                Image<Bgr, byte> image;
                

                using (var memoryStream = new MemoryStream(imageBytes))
                {
                    image = new Image<Bgr, byte>(new Bitmap(memoryStream));
                }

                Rectangle[] faces = FaceComparerVariables.FaceComparer.DetectFaces(image);

                if (faces.Length != 1)
                {
                    result = Json(new FindSiblingsResult()
                    {
                        Message = "FaceDetectionError"
                    });
                }
                else
                {
                    var applicationPath = HttpRuntime.AppDomainAppPath;
                    var imagesFolder = ConfigurationManager.AppSettings["ImagesFolder"];

                    List<string> clusterLabels = FaceComparerVariables.FaceComparer.FindSiblings(image, faces, int.Parse(ConfigurationManager.AppSettings["NumberOfClusters"]));

                    var imagesForResonse = new string[clusterLabels.Count];

                    for (int i = 0; i < clusterLabels.Count; i++)
                    {
                        var userImages = Directory.GetFiles(String.Format("{0}{1}\\{2}", applicationPath, imagesFolder, clusterLabels[i]));

                        if (userImages.Length > 0)
                        {
                            int imageIndex = random.Next(userImages.Length - 1);

                            using (var memoryStream = new MemoryStream())
                            {
                                var userImage = new Image<Bgr, byte>(userImages[imageIndex]);
                                userImage.Bitmap.Save(memoryStream, ImageFormat.Jpeg);

                                imagesForResonse[i] = Convert.ToBase64String(memoryStream.ToArray());
                            }
                        }
                    }

                    result = Json(new FindSiblingsResult()
                    {
                        Message = "OK",
                        Images = imagesForResonse
                    });

                    var userDirectoryPath = String.Format("{0}{1}\\{2}", applicationPath, imagesFolder, User.Identity.GetUserId());

                    if (!Directory.Exists(userDirectoryPath))
                    {
                        Directory.CreateDirectory(userDirectoryPath);
                    }

                    image.Save(String.Format("{0}/{1}.jpg", userDirectoryPath, Directory.GetFiles(userDirectoryPath).Length + 1));

                    FaceComparerVariables.FaceComparer.AddCluster(image, faces, User.Identity.GetUserId());

                    image.Dispose();
                }
            }

            return result;
        }

        public void Save()
        {
            var applictionPaht = HttpRuntime.AppDomainAppPath;
            string pathToFile = ConfigurationManager.AppSettings["PathToFiles"];

            FaceComparerVariables.FaceComparer.Save(applictionPaht + pathToFile);
        }
    }
}
