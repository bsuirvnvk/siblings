﻿using CompareFace;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace Siblings.Global
{   
    public static class FaceComparerVariables
    {
        public static FaceComparer FaceComparer { get; private set; }

        /// <summary>
        ///     Static constructor for singleton. Verifies the existence of serialized file. 
        ///     Reads all path from web.config file. Creates and teaches FaceComparer if needed. 
        /// </summary>
        static FaceComparerVariables()
        {
            int maxNumberOfEigenfaces; 
            float treshold;

            string pathToTrainDirectory = ConfigurationManager.AppSettings["PathToTrainDirectory"];
            string pathToFiles = ConfigurationManager.AppSettings["PathToFiles"];
            string pathToDesigionTree = ConfigurationManager.AppSettings["PathToDesignTree"];

            if(string.IsNullOrEmpty(pathToTrainDirectory) || string.IsNullOrEmpty(pathToFiles) 
                || !int.TryParse(ConfigurationManager.AppSettings["MaxNumberOfEigenfaces"], out maxNumberOfEigenfaces)
                || !float.TryParse(ConfigurationManager.AppSettings["Treshold"], out treshold))
            {
                throw new ConfigurationErrorsException("Bad config");
            }

            var applicationPath = HttpRuntime.AppDomainAppPath;

            if (Directory.GetFiles(applicationPath + pathToFiles).Length > 0)
            {
                FaceComparer = FaceComparer.Load(applicationPath + pathToFiles, applicationPath + pathToDesigionTree);
            }
            else
            {
                FaceComparer = new FaceComparer(applicationPath + pathToTrainDirectory, applicationPath + pathToDesigionTree, maxNumberOfEigenfaces, treshold);
            }
        }
    }
}