﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareFace
{
    [Serializable]
    class Cluster
    {
        /// <summary>
        ///     Class contains list of projections of the images and label
        ///     that identifies each instance of the class.
        /// </summary>
        private readonly List<float[]> _projectionsList;
        public string Label { get; private set; }

        private Cluster() { }
        public Cluster(List<float[]> list, string label)
        {
            _projectionsList = list;
            Label = label;
        }

        /// <summary>
        ///     Calculates a distance between two projection vectors.
        /// </summary>
        /// <param name="firstProjection"></param>
        ///     First projection vector. Must be not null.
        /// <param name="secondProjection"></param>
        ///     Second projection vector. Must be not null.
        /// <returns>
        ///     Returns distance betveen input projection vectors.
        /// </returns>
        private double CalculateDistance(float[] firstProjection, float[] secondProjection)
        {
            if (firstProjection == null || secondProjection == null || firstProjection.Length == 0 || firstProjection.Length != secondProjection.Length)
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }

            using (var secondProjectionMatrix = new Matrix<float>(secondProjection))
            using (var firstProjectionMatrix = new Matrix<float>(firstProjection))
            {
                return CvInvoke.cvNorm(firstProjectionMatrix.Ptr, secondProjectionMatrix.Ptr, NORM_TYPE.CV_L2, IntPtr.Zero);
            }
        }

        /// <summary>
        ///     Method for adding projection to instance of Cluster class.
        /// </summary>
        /// <param name="projection">
        ///     Projection of new image. Must be not null.
        /// </param>
        public void Add(float[] projection)
        {
            if (projection == null || projection.Length == 0)
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }
            _projectionsList.Add(projection);
        }

        /// <summary>
        ///     Method for adding range projection to instance of Cluster class
        /// </summary>
        /// <param name="projections">
        ///     List of projections of new images. Must be not null.
        /// </param>
        public void AddRange(List<float[]> projections)
        {
            if (projections == null)
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }
            _projectionsList.AddRange(projections);
        }

        /// <summary>
        ///     Method treats projection vectors and finds a minimal distance among all distances/
        /// </summary>
        /// <param name="inputProjection">
        ///     Projection of image.
        /// </param>
        /// <returns> Minimal distance. </returns>
        public double MinimalDistance(float[] inputProjection)
        {
            return _projectionsList.Select(x => CalculateDistance(inputProjection, x)).Min();
        }

    }
}
