﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Runtime.Serialization;

namespace CompareFace
{
    [DataContract]
    public class FaceComparer
    {
        public const string FaceComparerFile = @"\face_comparer.xml";
        public const string EigenFile = @"\eigen_objects.xml";

        public const int FaceImageSize = 128;

        [DataMember]
        private readonly List<Cluster> _clustersList = new List<Cluster>();

        private CascadeClassifier _haarFeaturesClassifier;

        private EigenObjectRecognizer _eigenObjectRecognizer;

        /// <summary>
        ///     Empty constructor for xml serialization
        /// </summary>
        private FaceComparer() {}

        /// <summary>
        ///     Initializes FaceComparer object. Makes eigenfaces basis from specified path considering max value of eigenfaces
        ///     entering to basis and treshold.
        /// </summary>
        /// <param name="pathToTrainDirectory">
        ///     Specify path to directory with images representing eigenfaces basis. Must be filled.
        /// </param>
        /// <param name="pathToDesigionTree">
        ///     Path to file using to train CasscadeClassifier
        /// </param>
        /// <param name="maxNumberOfEigenfaces">
        ///     Max value of eigenfaces entering to basis.  Must be equel or greater then zero. Zero means all eigenfaces.
        /// </param>
        /// <param name="treshold">
        ///     The value directs the algorithm in choise beetwen feature eigenfaces. Must be greater than null.
        /// </param>
        public FaceComparer(string pathToTrainDirectory, string pathToDesigionTree, int maxNumberOfEigenfaces, double treshold)
        {
            if (String.IsNullOrEmpty(pathToTrainDirectory) || maxNumberOfEigenfaces < 0 || treshold < 0)
            {
                throw new ArgumentException("Invalid arguments. See constructor description");
            }

            var termCriteria = new MCvTermCriteria(maxNumberOfEigenfaces, treshold);
            _eigenObjectRecognizer = new EigenObjectRecognizer(ReadBasisFromPath(pathToTrainDirectory, FaceImageSize, FaceImageSize).ToArray(), ref termCriteria);
            _haarFeaturesClassifier =  new CascadeClassifier(pathToDesigionTree);
        }


        /// <summary>
        ///     Adds new classter from specified image and faces on it.
        /// </summary>
        /// <param name="image">
        ///     Image for processing.   Must be not null.
        /// </param>
        /// <param name="faces">
        ///     Set of faces. Must be not null and filled in.
        /// </param>
        /// <param name="label">
        ///      Identificator of cluster. Must be filled and not null.
        /// </param>
        public void AddCluster(Image<Bgr, byte> image, Rectangle[] faces, string label)
        {

            if (image == null || faces == null || faces.Length == 0 || String.IsNullOrEmpty(label))
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }

            List<Image<Gray, byte>> faceImages =  CutFacesFromImage(image, faces, FaceImageSize, FaceImageSize);

            var projections = new List<float[]>(faceImages.Count);

            projections.AddRange(faceImages.Select(MakeBasisProjection));

            AddCluster(projections, label);
        }

        /// <summary>
        ///     Method adds new cluster with specified projection set 
        ///     or if cluster with such label already exists - adds range of new projections.
        /// </summary>
        /// <param name="newClusterProjections">
        ///     Input list of images projections. Must be not null.
        /// </param>
        /// <param name="label">
        ///     Identificator of cluster. Must be filled and not null.
        /// </param>
        private void AddCluster(List<float[]> newClusterProjections, string label)
        {
            if (newClusterProjections == null || string.IsNullOrEmpty(label))
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }

            if (!_clustersList.Exists(x => x.Label == label))
            {
                _clustersList.Add(new Cluster(newClusterProjections, label));
            }
            else
            {
                _clustersList.Find(x => x.Label == label).AddRange(newClusterProjections);
            }
            
        }

        /// <summary>
        ///     Reads images from specified directory, convert them to Grayscale.
        /// </summary>
        /// <param name="pathToTrainDirectory"></param>
        ///     Specify path to directory with images representing eigenfaces basis. Must be filled.
        /// <param name="height">
        ///     Height of output images. Must be greater than 0.
        /// </param>
        /// <param name="width">
        ///     Width of output images. Must be greater than 0.
        /// </param>
        /// <returns>
        ///     A list of histogram equalized grayscale image with specified size.
        /// </returns>
        private List<Image<Gray, Byte>> ReadBasisFromPath(string pathToTrainDirectory, int width, int height)
        {
            if (String.IsNullOrEmpty(pathToTrainDirectory) || height <= 0 || width <= 0)
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }

            string[] pathsToImages = Directory.GetFiles(pathToTrainDirectory);

            if (pathsToImages.Length == 0)
            {
                throw new FileNotFoundException("Train directory is empty");
            }

            var basisImages = new List<Image<Gray, byte>>(pathsToImages.Length);

            foreach (var pathToImage in pathsToImages)
            {
                using (var image = new Image<Gray, byte>(pathToImage).Resize(width, height, INTER.CV_INTER_CUBIC))
                {
                    var histogramEqualizedImage = new Image<Gray, byte>(width, height);
                    CvInvoke.cvEqualizeHist(image, histogramEqualizedImage);

                    basisImages.Add(histogramEqualizedImage);
                }
            }
            
            return basisImages;
        }

        /// <summary>
        ///     Makes the image projection on the eigenfaces.
        /// </summary>
        /// <param name="inputImage">
        ///     Image for processing. Must be not null and must have same width with basis images.
        /// </param>
        /// <returns>
        ///     Projection on the egenfaces. 
        /// </returns>
        public float[] MakeBasisProjection(Image<Gray, byte> inputImage)
        {
            if (inputImage == null || inputImage.Width != FaceImageSize || inputImage.Height != FaceImageSize)
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }

            return EigenObjectRecognizer.EigenDecomposite(inputImage, _eigenObjectRecognizer.EigenImages,
                _eigenObjectRecognizer.AverageImage);
        }

        /// <summary>
        ///     Detects faces on input image.
        /// </summary>
        /// <param name="inputImage"></param>
        ///     Image for processing.
        /// <returns>
        ///     Array of rectangles describe faces placement.
        /// </returns>
        public Rectangle[] DetectFaces(Image<Bgr, byte> inputImage)
        {
            Rectangle[] faces = (inputImage != null) ? _haarFeaturesClassifier.DetectMultiScale(inputImage.Convert<Gray, byte>(), 1.1, 15, new Size(20, 20), Size.Empty) : null;

            return faces;
        }

        /// <summary>
        ///     Cuts fragments from input image.
        /// </summary>
        /// <param name="inputImage">
        ///     Image for processing. Must be not null.
        /// </param>
        /// <param name="faces">
        ///     Array of rectangles describe faces placement. Must be not null. Rectangles Height and width must be greater than 0; 
        /// </param>
        /// <param name="width">
        ///     Width of output images. Must be greater than 0.
        /// </param>
        /// <param name="height">
        ///     Height of output images. Must be greater than 0.
        /// </param>
        /// <returns>
        ///     Return central images with specified size containing needed fragment.
        /// </returns>
        private List<Image<Gray, byte>> CutFacesFromImage(Image<Bgr, byte> inputImage, Rectangle[] faces, int width,
            int height)
        {
            if (inputImage == null || height <= 0 || width <= 0 || faces == null || faces.Any(rect => rect.Width <= 0 || rect.Height <= 0))
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }

            var result = new List<Image<Gray, byte>>(faces.Length);

            foreach (var face in faces)
            {
                inputImage.ROI = face;
                var faceImage = inputImage.Copy().Convert<Gray, byte>().Resize(width, height, INTER.CV_INTER_CUBIC);

                result.Add(faceImage);
            }

            inputImage.ROI = new Rectangle(0, 0, 0, 0);

            return result;
        }

        /// <summary>
        ///     Cuts fragments from input image.
        /// </summary>
        /// <param name="inputImage">
        ///     Image for processing. Must be not null.
        /// </param>
        /// <param name="faces">
        ///     Array of rectangles describe faces placement. Must be not null. Rectangles Height and width must be greater than 0; 
        /// </param>
        /// <param name="size">
        ///     Size of output images. Height and width must be greater than 0.
        /// </param>
        /// <returns>
        ///     Return central images with specified size containing needed fragment.
        /// </returns>
        private List<Image<Gray, byte>> CutFacesFromImage(Image<Bgr, byte> inputImage, Rectangle[] faces, Size size)
        {
            return CutFacesFromImage(inputImage, faces, size.Width, size.Height);
        }

        /// <summary>
        ///     Method finds 10 minimal distances between cluster and every face on input image. 
        /// </summary>
        /// <param name="image">
        ///     Input image. Must be not null.
        /// </param>
        /// <param name="faces">
        ///     Array of rectangles describe faces placement. Must be not null. Rectangles Height and width must be greater than 0.
        /// </param>
        /// <param name="numberOfClusters">
        ///      Number of clusters that FindSiblings returns.
        /// </param>
        /// <returns>
        ///     Returns list with labels of all founded clusters.
        /// </returns>
        public List<string> FindSiblings(Image<Bgr, byte> image, Rectangle[] faces, int numberOfClusters) 
        {
            List<Image<Gray, byte>> imagesList = CutFacesFromImage(image, faces, FaceImageSize, FaceImageSize);
            var result = new List<string>();

            foreach (Image<Gray, byte> img in imagesList)
            {
                CvInvoke.cvEqualizeHist(img, img);
                float[] projection = MakeBasisProjection(img);
                _clustersList.Sort((x, y) => (int)(x.MinimalDistance(projection) - y.MinimalDistance(projection)));
                result.AddRange(_clustersList.Take(numberOfClusters).Select(x => x.Label));               
            }

            return result;
        }

        /// <summary>
        ///     Method for xml serialization.
        /// </summary>
        /// <param name="path">
        ///     Path to the directory with save files. Can't be null.
        /// </param>
        public void Save(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }

            var serializer = new DataContractSerializer(typeof(FaceComparer));
            using (var stream = new FileStream(path + FaceComparerFile, FileMode.Create, FileAccess.Write))  
            {
                serializer.WriteObject(stream, this);
            }

            var xmlSerializer = new XmlSerializer(typeof(EigenObjectRecognizer));
            using (var stream = new FileStream(path + EigenFile, FileMode.Create, FileAccess.Write))
            {
                xmlSerializer.Serialize(stream, _eigenObjectRecognizer);
            }
        }

        /// <summary>
        ///     Method for xml deserialization.
        /// </summary>
        /// <param name="path">
        ///     Path to the directory with save files. Can't be null.
        /// </param>
        /// <param name="pathToDesigionTree">
        ///     Path to file using to train CasscadeClassifier
        /// </param>
        /// <returns>
        ///     Returns instance of FaceComparer class.
        /// </returns>
        public static FaceComparer Load(string path, string pathToDesigionTree)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("Invalid arguments. See method description");
            }

            FaceComparer result;

            var serializer = new DataContractSerializer(typeof(FaceComparer));
            using (var stream = new FileStream(path + FaceComparerFile, FileMode.Open, FileAccess.Read))
            {
                result = (FaceComparer)serializer.ReadObject(stream);
            }

            var xmlSerializer = new XmlSerializer(typeof(EigenObjectRecognizer));
            using (var stream = new FileStream(path + EigenFile, FileMode.Open, FileAccess.Read))
            {
                result._eigenObjectRecognizer = (EigenObjectRecognizer)xmlSerializer.Deserialize(stream);
            }

            result._haarFeaturesClassifier = new CascadeClassifier(pathToDesigionTree);

            return result;
        }
    }
}
